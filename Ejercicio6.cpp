#include <stdio.h>
#include<stdint.h>

#define TAM 10
int main(void){
	int array[10];
	//Init array
	for (int32_t i=0;i<TAM; i++){
		array[i]=i;
	}
	//print array
	for (int32_t i=0;i<TAM; i++){
		printf(" [%d]",array[i]);
	}
	printf("\n");
	int *pPointer=&array[3];
	*pPointer=300;
	//Print array
	for (int32_t i=0;i<TAM; i++){
		printf(" [%d]",array[i]);
	}
	printf("\n");
	
	for (int32_t i=0;i<TAM; i++){
		printf(" [%d]",*(pPointer+i));
	}
 printf("\n");
return 0;}  
