#include <stdio.h>
#include <stdint.h>
#define TAM 10

int main(void){
int array[10];
//INIT ARRAY
for(int32_t i = 0; i < TAM; i++){
array[i] = i;
	
}
//imprimir array
for(int32_t i = 0; i<TAM; i++){
	printf("[%d]",array[i]);
	
}
printf("\n");
array[5]= 500;
int *pPointer = array;//VALIDO
//imprimir array
for(int32_t i= 0; i<TAM; i++){
	printf("[%d]",*(pPointer+i));
	pPointer++;//avanzo en direcciones
}

printf("\n");
array[8]= 800;
//inicializar el puntero el puntero "pPointer"
pPointer = array;
//imprimir array
for(int32_t i= 0; i<TAM; i++){
	printf("[%d]",*(pPointer+i));
}
printf("\n");
return 0;
}
