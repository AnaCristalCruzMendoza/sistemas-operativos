#include<stdio.h>
int main (void){
	int *pPuntero;
	int var=10;
	pPuntero=&var;
	printf("Before==> Valor de var es: [%d]\n",var);
	printf("Before==> Valor de pPubtero es: [%d]\n",*pPuntero);
	
	var=20;
	printf("After==> Valor de var es: [%d]\n",var);
	printf("After==> Valor de pPubtero es: [%d]\n",*pPuntero);
	
	return 0;
}
