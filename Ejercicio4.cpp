#include <stdio.h>
#include<stdint.h>

#define TAM 10
int main(void){
	int array[10];
	//Init array
	for (int32_t i=0;i<TAM; i++){
		array[i]=i;
	}
	//print array
	for (int32_t i=0;i<TAM; i++){
		printf(" [%d]",array[i]);
	}
	printf("\n");
	array[5]=500;
	int *pPointer= array;//valido
	//Print array
	for (int32_t i=0;i<TAM; i++){
		printf(" [%d]",*(pPointer+i));
		pPointer++;
	}
	printf("\n");
	array[8]=800;
	//Sin Inicializar una vez mas el puntero "pPointer"
	//Print array
	for (int32_t i=0;i<TAM; i++){
		printf(" [%d]",*(pPointer+i));
	}
 printf("\n");
return 0;}  
